document.getElementById("btn-1").addEventListener('click',
	()=> {alert("Add More!");
});
let paragraph = document.getElementById("paragraph-1");
let paragraph2 = document.getElementById("paragraph-2");

document.getElementById("btn-2").addEventListener('click',()=> {
	paragraph.innerHTML = "I can even do this!";
});
document.getElementById("btn-3").addEventListener('click',() => {
	paragraph2.innerHTML = "I purple you!";
	paragraph2.style.color = "purple";
	paragraph2.style.fontSize="50px";
});

/*Lesson Proper
Topics
-Introduction
-Writing comments
-Syntax and Statements
-Storring Values through variables
-Peeking at Variable's value through console
-Data types
-Functions

JavaScript
-is a scripting language that enables you to make interactive web pages
-it is one core technologies of the World Wide Web 
-was originally intended for web browsers. However, they are now also integrated in web servers through the use of Node.js.

Use of JavaScript
-Web app development
-Browser-based game development
- Art Creation
- Mobile applications
*/

// Writing Comments in JavaScript:
//There are two ways of writing comments in JS:
/*
// - single line comments - ctrl + /
	// sample comments - can only make comments in single line
*/

/*
multi line comments:
ctrl + shift + /
comments in JS, much like CSS and HTML, is not read by the browser. So, these comments are often used to add notes and to add markers to your code.

*/

console.log("Hello, my name is Kathy. Ayaw ko na mabuhay.");

/*
JavaScript
we can see or log message in our console.

-Consoles are part of our browser which will allow us to see/log messages, data or information from out programming language

- for most browsers, console can be accessed through its developer tools in console tab

In fact, console in browser allow us to add some JavaScript expression.

Statements
- Statements are instructions, expression we add to our programming language which will then be communicated to our computers

-Statement in JavaScript commonly ends in semi-colon(;). However, JavaScript has an implemented way of automatically adding semicolons at the end of our statements. Which, therefore, mean that, unlike other languages, JS does NOT require semicolons.

- Semicolons in js are monstly used to mark the end of the statement

Syntax
- Syntax in programming, is a set of rules that describes how statements are properly made/constructed.
- Lines/block of codes mus follow a certain rules for it to work. Because remember, you are not merely communicating with another human, in fact you are communicating with a computer
*/

console.log("Kathy");

/*Variables*/
/* 
	In HTML, element are containers of other element and text.
	In Javascript, variables are containers of data. A given name is used to describe that piece of data.

	Variables also allows us to use or refer to data multiples times.
*/

//num is our variable
// 10 being the value/data
let num = 10; 
console.log(num);

let name = "Jin";
console.log("V");
console.log(name);

/*Creating Variables */
/*
	To create a variable, there are two steps to be done:
		-Declaration = which actually allows to create the variable.
		-Intiallization = which allows to add inital value to a variable.
	Variables in JS are declared with the use of let or const keyword.
*/
let myVariable;
/* 
	We can create variables without an initial value.
	However, when logged into the console, the variable will return a value of undefined.

	Undefined is a datatype that indcates that variable does exist, however there was no intial vallue
*/
console.log(myVariable)

myVariable = "New Initialized Value"
console.log(myVariable)

/* 
	myVairbable2 = "Initial Value";
	let myVariable2;
	console.log(myVariable2);

	Not: You cannot and should not access a variable before it's been created/declared

	Con you use or refer to variable that has not been declared or created?
		NO. this wil result in an error. not defined

		undefined vs not defined
		undefined means a variable has been declarewd but there is no inital value
			undefined is a data type
		not defined means that the variable you are trying to refer or access does not exist
			not defined is an error

		note: some errors is js will stop the program from further executing.
*/
let myVariable3="Another sample";
console.log(myVariable3);
/*
	Variables must be declared first before they are used referred to or accesed.
	Using, referring to or accessing a variable before it's been declared results an error.
*/
//let vs Const
/*
	with the use of let keyword, we can create variable that can be declared, initialized and reassigned.
	in fact, we can declare let variables and initialize after
*/
let bestFinalFantasy;
bestFinalFantasy ="Final Fantasy 6";
console.log(bestFinalFantasy);

/*re-assigning let variables*/
bestFinalFantasy = "Final fantasy 7";
console.log(bestFinalFantasy);
/*Did the value change? Yes. Can we re-assign values to let variables*/
const pi = 3.1416;
console.log(pi);

/*
	const variables are variables with constant data. Therefore we should not re-declare, reassign or even declare it.
	const variable are used for data 
*/
//Can you re-assign another valye to a const variable? No. An error will occur.

/*Guides on Variable Names
	1. When naming variables, it is important to create variables, it is important to create variables that are descriptive and indicate of data it contains.
		let firstname = "Kathy";
		let pokemon = 25000 (bad variable)
	2. When naming variables, it is better to start with a lowercase letter. We usually aviod creating variables names starts with capital letters, because there are several keywords in js that start in capital letter
		let firstName = "Jhope";
		let FirstName = "Michael";
	3. Do not add to your variable names. Use camelCase for multiple words for multiple words.underscore
		let firstname = "Mike";

		camelCase: lastName emailAddress mobileNumber
		under_scores: let product_description = "lorem ipsum"
*/
let num_sum = 5000;
let numSum =6000;
console.log(num_sum);
console.log(numSum);

// Declaring multiple variables
let brand = "Toyota",model = "Vios", type = "Sedan";
console.log(brand);
console.log(model);
console.log(type);

//console logging multiple variables: use commas to separate each value
console.log(brand,model,type)

/*
Data Types
	In most programming languages, data is differentiated by their types. For most programming languages,
	you have to declare not the variable name but also the type of data you are sabing into a variable. However, JS does not require this.

	To create data with particular data types, some data types require adding with literal.
		string literals = '',"", and most recentaly : ``(template literals)
		object literals = {}
		array literals = []
*/
/*String*/
	// Strings are a series of alphanumeric that create a word, a phrase, a name or anything related to creating text
	// String literals such ''(single quote) or ""(double quote)are used to write/ create strings

/* Mini-Activity:
	let firstname = "Katherine";
let lastname = "Cristal";

console.log(firstname,lastname);
*/

//Concatenation is process/operation wherein we combine two strings as one. Using the plus(+) sign.
//JS strings, spaces are also counted as characters

/* Mini-Activity

let firstname = "Katherine";
let lastname = "Cristal";

console.log(firstname,lastname);


let fullName = firstname + " " + lastname;

let word1 = "is";
let word2 = "student";
let word3 = "of";
let word4 = "University";
let word5 = "De La Salle";
let word6 = "a";
let word7 = "Dasmariñas";
let space = " ";

let sentence = fullName + space + word1 + space + word6 
	+ space + word2 + space + word3 + space 
	+ word5 + space + word4 + space + word7;
console.log(sentence);
*/

let sentence = `${fullName} ${word1} ${word6} ${word2} ${word3} ${word5} ${word4} ${word7}`;
console.log(sentence);

/*Template literals(``) will allow us to create string with the use of backticks. Template literals also allows us to easily concatenate strings without the use of +(plus).
This is also allow to embed or add variable and even expressions in our string with the use of placeholders ${}. */

//Number (Data type)
	// Integers (whole numbers) and floats(decimals). These are our number data which can be used for mathematical operations

 let numString1 = "5";
 let numString2 = "6";
 let num1 = 5;
 let num2 = 6;

 console.log(numString1 + numString2); //56 strings were concatenated
 console.log(num1+num2); //11 = both argument in the operation are number types

 let num3 = 5.5;
 let num4 = .5;
 console.log(num1 + num2);
 console.log(num3 + num4);

 /*When the + or addition operator is used on numbers, it will do the proper mathematical operation.
 However, when used on strings, it will concatenate. */

console.log(numString1 + num1); // 55 = resulted in concatenation
console.log(num3 + numString2);
//Forced coercion - when one data's type is forced to change to comeplete an operation
//string + num = concatenation

//parseInt() - this can change the type of numeric string to a proper number
console.log(parseInt(numString1)+num1);//10 = numString1 was parsed into a proper number



